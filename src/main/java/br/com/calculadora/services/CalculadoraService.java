package br.com.calculadora.services;

import br.com.calculadora.dto.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import org.springframework.stereotype.Service;

@Service
public class CalculadoraService {

    public RespostaDTO somar(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()) {
            resultado = resultado + numero;
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

    public RespostaDTO subtrair(Calculadora calculadora) {
        int resultado = 0;
        for (Integer numero : calculadora.getNumeros()) {
            resultado = ((numero) - resultado);
        }
        RespostaDTO respostaDTO = new RespostaDTO(resultado);
        return respostaDTO;
    }

        public RespostaDTO multiplicar(Calculadora calculadora){
            int resultado = 1;
            for (Integer numero : calculadora.getNumeros()) {
                resultado = resultado * numero;
            }
            RespostaDTO respostaDTO = new RespostaDTO(resultado);
            return respostaDTO;
        }

            public RespostaDTO dividir(Calculadora calculadora) {
            int resultado = 1;
            for (Integer numero : calculadora.getNumeros()) {
                resultado = ((numero) - resultado);
            }
            RespostaDTO respostaDTO = new RespostaDTO(resultado);
            return respostaDTO;
        }

}
