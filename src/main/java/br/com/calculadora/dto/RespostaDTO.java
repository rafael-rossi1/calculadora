package br.com.calculadora.dto;

public class RespostaDTO {
    private Integer resultado;

    public RespostaDTO(Integer resultado) {
        this.resultado = resultado;
    }

    public RespostaDTO() {
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }
}
