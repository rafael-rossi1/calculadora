package br.com.calculadora.controllers;

import br.com.calculadora.dto.RespostaDTO;
import br.com.calculadora.models.Calculadora;
import br.com.calculadora.services.CalculadoraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.xml.ws.Response;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora) {
        if(calculadora.getNumeros().size()<=1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário 2 numeros");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora) {
        if(calculadora.getNumeros().size()<=1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário 2 numeros");
        }
        return calculadoraService.subtrair(calculadora);
    }
    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora) {
        if(calculadora.getNumeros().size()<=1) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Necessário 2 numeros");
        }
        return calculadoraService.multiplicar(calculadora);
    }

}
